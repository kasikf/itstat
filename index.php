<?php 
    require_once("vendor/autoload.php");
    $latte = new Latte\Engine;
    $latte->setTempDirectory('temp');

    $rows = [];
    $pololeti = [];
    $prumery = [];
    $vysledek = -1;

    $file = fopen("stats.csv","r");
    while (($data = fgetcsv($file))!== FALSE) {
        array_push($rows, $data);
    }
    fclose($file);

    //uložení názvů pololetí
    foreach ($rows[0] as $key => $value) {
        if ($key !== 0) {
            array_push($pololeti, $value);
        }
    }

    //spočítání průměrů
    for ($i = 1; $i <= count($pololeti); $i++) {
        $pocet = 0;
        $soucet = 0;

        for ($j = 1; $j < count($rows); $j++) {
                $pocet++;
                $soucet += $rows[$j][$i];
        }

        $prumer = round($soucet/$pocet, 2);
        array_push($prumery, $prumer);
    }

    if(isset($_POST["submit"])){
        $pololetiSelect = $_POST["pololeti"];

        if ($pololetiSelect || $pololetiSelect > -1) {
            $vysledek = $prumery[$pololetiSelect];
        }
    }

    $params = [
        'pololeti' => $pololeti,
        'vysledek' => $vysledek
    ];
    
    

    $latte->render('templates/stats.latte', $params);
?>