<?php

use Latte\Runtime as LR;

/** source: templates/stats.latte */
final class Template9d2707e992 extends Latte\Runtime\Template
{
	public const Source = 'templates/stats.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    
    <link rel="stylesheet" href="style.css">
    
</head>
<body>
    <main>
        <div class="container">
            <h1>Průměr pololetí ITStat</h1>

            <form action="index.php" method="post">
                <div class="form-group">
                    <label for="pololeti-select">Vyber&nbsp;pololetí</label>
                    <select name="pololeti" id="pololeti-select">
                        <option value="" selected>--Vyber z nabídky--</option>

';
		for ($i = 0;
		$i < count($pololeti);
		$i++) /* line 22 */ {
			echo '                        <option value="';
			echo LR\Filters::escapeHtmlAttr($i) /* line 23 */;
			echo '">';
			echo LR\Filters::escapeHtmlText($pololeti[$i]) /* line 23 */;
			echo '</option>
';

		}
		echo '                    </select>
                </div>

                <button type="submit" id="submit" name="submit"> Spočítej&nbsp;průměr</button>
            </form>

';
		if (isset($vysledek) && $vysledek !== -1) /* line 31 */ {
			echo '            <div class="result">   
                ';
			echo LR\Filters::escapeHtmlText($vysledek) /* line 32 */;
			echo '
            </div>
';
		}
		echo '        </div>

        

    </main>
</body>
</html>';
	}
}
